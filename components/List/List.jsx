import React from 'react';
import { observer } from "mobx-react";
import ListStore from './ListStore';
import ListItem from "../ListItem/ListItem";

@observer
class List extends React.Component {
	constructor(props){
		super(props);
		this.store = new ListStore(); 
	}

	render() {
		const { items } = this.store;

		return (
			<div className="list-container">
				<ul className="list">
					{ items.map(item => (<li className="list-item" key={item.id}>{item.text}</li>)) }
				</ul>
			</div>
		)
	}
}

export default List;