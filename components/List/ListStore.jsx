import { observable } from 'mobx';

class ListStore {
	@observable items;

	constructor(props) {
		this.items = [
			{ id:1, text: 'this is item number one'},
			{ id:2, text: 'this is item number two'},
			{ id:3, text: 'this is item number three'}
		]
	}
}

export default ListStore;