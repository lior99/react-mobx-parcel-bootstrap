import React from 'react';

const ListItem = (props) => (<li className={props.className} key={props.id}>{props.text}</li>);

export default ListItem;