import React from 'react';
import { render } from 'react-dom';
import { mobx } from 'mobx';
import List from './components/List/List';
import ListItem from "./components/ListItem/ListItem";
import './style/main';

class App extends React.Component {
	render() {
		return (
			<React.Fragment>
				<div className="container">
					Welcome to Parcel bootstrap! 
				</div>
				<List render={(items) => {
					{ items.map(item => {
						return (<ListItem {...item} />)
					})}
					}}
				/>
			</React.Fragment>

		)
	}

}

render(
	<App />,
	document.querySelector('#output')
);